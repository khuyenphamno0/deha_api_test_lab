<?php

namespace App\Http\Service\Category;

use App\Models\Category;

class CategoryService 
{
    protected $category;
    public function __construct( Category $category)
    {
        $this->category = $category;
    }
    public function index()
    {
        return $this->category->paginate(10);
    }
    public function show($id)
    {
        return $this->category->findOrfail($id);
    }
    public function create( $data )
    {
        return $this->category->create($data);
    }
    public function update( $id , $data)
    {
        $category = $this->category->findOrFail($id);
        $category->update($data);
        return $category;
    }
    public function destroy($id)
    {
        $category = $this->category->findOrFail($id);
        $category->delete();
        return true;
    }
}