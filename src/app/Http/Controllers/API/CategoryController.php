<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryCreateRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Service\Category\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected $categoryService;
    public function __construct( CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = $this->categoryService->index();
        $categoryResource = new CategoryCollection($categories);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Get Successfull',
            'data' => $categoryResource
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryCreateRequest $request)
    {
        $category = $this->categoryService->create($request->input());
        $categoryResource = new CategoryResource($category);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Created Successfull',
            'Category' => $categoryResource
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $category = $this->categoryService->show($id);
        $categoryResource = new CategoryResource($category);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Found Successfull',
            'Category' => $categoryResource
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryUpdateRequest $request, string $id)
    {
        $category = $this->categoryService->update( $id, $request->input());
        $categoryResource = new CategoryResource($category);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'update successfull',
            'Category' => $categoryResource
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->categoryService->destroy($id);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Delete Successfull'
        ]);
    }
}
