$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// Load page
$(document).ready(function() {
    getlistCategory(1)
    document.getElementById('currentPage').innerHTML = 1
    document.getElementById('editButton').disabled = true
    document.getElementById('deleteButton').disabled = true
})
//Get list of category with Paginate
function getlistCategory(page)
{
    $.ajax({
        type: 'GET',
        url: '/api/categories/?page='+page,
        dataType: 'JSON',
        success: function(response) {
            categories = response.data.Categories
            categories.forEach(category => {
                var totalPage = response.data.meta.totalPage
                document.getElementById('totalPage').textContent = totalPage
                $('#listCategory').append('<tr id="'+category.id+'">\
                    <td>'+category.id+'</td>\
                    <td>'+category.name+'</td>\
                    <td style="text-align:center">'+category.totalQuantity+'</td>\
                    <td style="text-align:center"><button onclick="showCategory('+category.id+')"><i class ="fa fa-info-circle"></i></button></td>\
                    </tr>'
                )
            });
        }
    })
}

//Goto the next Page
function nextPage()
{   
    var page = document.getElementById('currentPage').textContent
    var totalPage = document.getElementById('totalPage').textContent
    if ( page<totalPage ) {
        page++
        document.getElementById('currentPage').textContent = page
        var table = document.getElementById('listCategory')
        table.innerHTML = table.rows[0].outerHTML
        getlistCategory(page)
    }
}
//Goto Previous Page
function prevPage()
{
    var page = document.getElementById('currentPage').textContent
    if (page >=2){
        page--
        document.getElementById('currentPage').textContent = page
        var table = document.getElementById('listCategory')
        table.innerHTML = table.rows[0].outerHTML
        getlistCategory(page)
    }
}
//Show 1 Category
function showCategory(id)
{
    var name = document.getElementById('name')
    var totalQuantity = document.getElementById('totalQuantity')
    var description = document.getElementById('description')
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/api/categories/'+id,
        success: function(response) {
            document.getElementById('editButton').disabled = false
            document.getElementById('deleteButton').disabled = false
            document.getElementById('categoryID').textContent = response.Category.id
            name.value = response.Category.name
            totalQuantity.value = response.Category.totalQuantity
            description.value = response.Category.description
        }
    })
}
//Create new Category
function add_button()
{
    var name = $('#name').val()
    var totalQuantity = $('#totalQuantity').val()
    var description = $('#description').val()
    var data = {
        'name' : name,
        'totalQuantity' : totalQuantity,
        'description' : description
    }
    $.ajax({
        type:'POST',
        dataType: 'JSON',
        url: '/api/categories/',
        data: data,
        success: function(response) {
            Swal.fire(
                'Successfull',
                response.message,
                'success'
            )
        },
        error: function(response) {
            error =  JSON.parse( response.responseText)
            Swal.fire(
                'Failure',
                error.message,
                'error'
            )
        }
    })
}
//Update 1 category
function edit_button()
{
    var name = $('#name').val()
    var totalQuantity = $('#totalQuantity').val()
    var description = $('#description').val()
    var id = document.getElementById('categoryID').textContent
    var data = {
        'name' : name,
        'totalQuantity' : totalQuantity,
        'description' : description
    }
    $.ajax({
        type: 'PUT',
        data: data,
        url: '/api/categories/'+id,
        dataType: 'JSON',
        success: function(response) {
            Swal.fire(
                'Successfull',
                response.message,
                'success'
            )
        },
        error: function(response) {
            error = JSON.parse(response.responseText)
            Swal.fire(
                'Failure',
                error.message,
                'error'
            )
        }
    })
}
//Delete 1 Category
function delete_button()
{
    var id = document.getElementById('categoryID').textContent
    $.ajax({
        type: 'DELETE',
        dataType: 'JSON',
        url: '/api/categories/'+id,
        success: function(response) {
            Swal.fire(
                'Successfull',
                response.message,
                'success'
            ).then(function(){
                location.reload()
            })
        },
        error: function(response) {
            var error = response.responseText
            Swal.fire(
                'Failure',
                error.message,
                'error'
            )
        }
    })
}
function home_button()
{
    location.reload()
}