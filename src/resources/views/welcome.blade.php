<!DOCTYPE html>
<html lang="zxx">
    <head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div style=' height:600px; width:1000px;margin-left:150px;' >
            <h1 style='text-align:center'>
                Category Management
            </h1>
            <div style='display:flex'>
                <div style='width:500px;height:350px;margin-left:30px; border: solid 1px'>
                    <table id="listCategory" style='border-collapse: collapse;margin-left:20px' >
                        <tr style='background-color:#689fe8' >
                            <td style='width:70px'>ID</td>
                            <td style='width:150px' >Category Name</td>
                            <td style='width:100px'>TotalQuantity</td>
                            <td style='width:100px; text-align:center' >Chose</td>
                        </tr>

                    </table>
                    <div>
                        <div style="display:flex; position:absolute;bottom: 430px">
                            <button onclick="prevPage()" style="width: 30px; height:20px; margin-top:15px;margin-right:5px; background-color:#78d166" ><i class="fa fa-arrow-left" ></i></button>
                            <div style="display:flex">
                                <h3>Page &nbsp;</h3>
                                <h3 id="currentPage"></h3>
                                <h3>&nbsp; of &nbsp;</h3>
                                <h3 id="totalPage"></h3>
                            </div>
                            <button onclick="nextPage()" style="width: 30px; height:20px; margin-top:15px;margin-left:5px; background-color:#78d166" ><i class="fa fa-arrow-right" ></i></button>
                        </div>
                        
                    </div>
                </div>
                <div style='width:400px;height:450px; margin-left:30px;border:solid 1px;'>
                    <div style="display:flex">
                        <h3 style="margin-left: 20px">Do something with Category: </h3>
                        <h3 style="color:#f0624f;margin-left:20px" id="categoryID" ></h3>
                    </div>    
                    <form action="">
                        <div style='width: 360px;height:400px; margin:10px 0 0 20px; padding-top:20px' >
                            <div style='width: 320px; height:80px; margin-left:20px;display:flex'>
                                <h4>Category Name: </h4>
                                <input id='name' name='name' style='border:solid 1px black;width:200px;height:30px;margin-left:20px;margin-top:15px' type="text" placeholder='Category Name...'/>       
                            </div>
                            <div style='width: 320px; height:80px; margin-left:20px;display:flex'>
                                <h4 >Total Quantity:</h4>
                                <input id='totalQuantity' name='totalQuantity' style='border:solid 1px black;width:150px;height:30px;margin-left:20px;margin-top:15px' type="number" placeholder='Category TotalQuantity...'/>       
                            </div>
                            <div style='width: 320px; height:80px; margin-left:20px; display:flex'>
                                <h4 >Category Description:</h4>
                                <input id='description' name='description' style='border:solid 1px black;width:200px;height:30px;margin-left:20px;margin-top:15px' type="text" placeholder='Category Description...'/>
                            </div>
                            <div style='width: 320px; height:80px; margin-left:20px;display:flex'>
                                <button id="addButton" type="button" onclick="add_button()" style='background-color:#78d166;margin-left:20px;height:40px;width:80px;margin-top:5px;color:white'>Create</button>
                                <button id="editButton" type="button" onclick="edit_button()"style='background-color:#dbc33b;margin-left:20px;height:40px;width:80px;margin-top:5px;color:white'>Update</button>
                                <button id="deleteButton" type="button" onclick="delete_button()" style='background-color:#f0624f;margin-left:20px;height:40px;width:80px;margin-top:5px;color:white' >Delete</button>
                                <button id="homeButton" type="button" onclick="home_button()" style='background-color:#00688B;margin-left:20px;height:40px;width:80px;margin-top:5px;color:white' ><i class="fa fa-home" ></i></button>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
           
        </div>
    </body>
   
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="/JS/main.js"></script>
</html>